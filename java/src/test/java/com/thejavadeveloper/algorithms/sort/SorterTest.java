package com.thejavadeveloper.algorithms.sort;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for sort algorithms.
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 09:55
 */
public class SorterTest {

    // - class attributes -------------------------------------------

    private static final int[] UNSORTED_INT_ARRAY = new int[]{2, 5, 3, 9, 4, 0, 6, 1, 7, 8};
    private static final int[] SORTED_INT_ARRAY = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    // - test methods -----------------------------------------------

    @Test
    public void testInsertionSortAlgorithm() {
        checkSort(new InsertionSort());
    }

    @Test
    public void testMergeSortAlgorithm() {
        checkSort(new MergeSort());
    }

    @Test
    public void testHeapSortAlgorithm() {
        checkSort(new HeapSort());
    }

    @Test
    public void testQuickSortAlgorithm() {
        checkSort(new QuickSort());
    }

    @Test
    public void testCountingSortAlgorithm() {
        checkSort(new CountingSort());
    }

    @Test
    public void testTwoColorSortAlgorithm() {
        Sorter sorter = new TwoColorsSort();
        assertArrayEquals(new int[]{0, 1, 0, 1, 0, 1, 1, 1, 1, 1}, sorter.sort(new int[]{1, 1, 0, 1, 1, 0, 1, 1, 1, 0}));
        assertArrayEquals(new int[]{0, 1, 0, 1, 0, 1, 0, 0, 0, 0}, sorter.sort(new int[]{1, 0, 0, 0, 1, 0, 0, 0, 1, 0}));
        assertArrayEquals(new int[]{0, 1, 0, 1, 0, 1, 0, 0, 0, 0}, sorter.sort(new int[]{0, 1, 0, 0, 1, 0, 0, 0, 1, 0}));
    }

    private void checkSort(Sorter sorter) {
        assertArrayEquals(SORTED_INT_ARRAY, sorter.sort(Arrays.copyOf(UNSORTED_INT_ARRAY, UNSORTED_INT_ARRAY.length)));
    }

}
