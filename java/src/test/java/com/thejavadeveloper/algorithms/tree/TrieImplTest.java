package com.thejavadeveloper.algorithms.tree;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Trie tests.
 * User: manuel.martin
 * Date: 23/01/13
 * Time: 09:10
 */
public class TrieImplTest {

    private TrieImpl.PrefixSelector<String, String> prefixSelector = new TrieImpl.PrefixSelector<String, String>() {
        @Override
        public List<String> getPrefixes(String key) {
            List<String> result = new ArrayList<String>();

            if (key != null) {
                String[] prefixes = key.split("\\.");
                result = Arrays.asList(prefixes);
            }

            return result;
        }
    };

    @Test
    public void testTrie() {
        TrieImpl<String, String, String> trie = new TrieImpl<String, String, String>(this.prefixSelector);

        trie.put("java.lang.String", "java.lang.String");
        trie.put("java.lang.Boolean", "java.lang.Boolean");
        trie.put("java.lang.Byte", "java.lang.Byte");
        trie.put("java.lang.Character", "java.lang.Character");
        trie.put("java.lang.Number", "java.lang.Number");
        trie.put("java.lang.Object", "java.lang.Object");

        trie.put("java.lang.reflect.Array", "java.lang.reflect.Array");
        trie.put("java.lang.reflect.Constructor", "java.lang.reflect.Constructor");
        trie.put("java.lang.reflect.Field", "java.lang.reflect.Field");
        trie.put("java.lang.reflect.Method", "java.lang.reflect.Method");

        trie.put("javax.security.auth.Subject", "javax.security.auth.Subject");
        trie.put("javax.security.auth.Policy", "javax.security.auth.Policy");
        trie.put("javax.security.auth.AuthPermission", "javax.security.auth.AuthPermission");

        Collection<TrieImpl.TrieEntry<String, String>> collection = trie.get("java");
        assertEquals(10, collection.size());

        collection = trie.get("javax");
        assertEquals(3, collection.size());

        collection = trie.get("java.lang.reflect");
        assertEquals(4, collection.size());

        collection = trie.get("java.beans");
        assertEquals(0, collection.size());

        collection = trie.get("com.java");
        assertEquals(0, collection.size());

    }

}
