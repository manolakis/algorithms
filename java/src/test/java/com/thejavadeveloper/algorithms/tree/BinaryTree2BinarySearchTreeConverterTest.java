package com.thejavadeveloper.algorithms.tree;

import org.junit.Test;

import static com.thejavadeveloper.algorithms.tree.BinaryTree2BinarySearchTreeConverter.Node;
import static org.junit.Assert.assertEquals;

/**
 * BinaryTree to BinarySearchTree converter test.
 * User: manuel.martin
 * Date: 17/01/13
 * Time: 22:37
 */
public class BinaryTree2BinarySearchTreeConverterTest {

    @Test
    public void testConvert() throws Exception {

        Node<Integer> node7 = new Node<Integer>(7, null, null);
        Node<Integer> node6 = new Node<Integer>(6, null, null);
        Node<Integer> node5 = new Node<Integer>(5, null, null);
        Node<Integer> node4 = new Node<Integer>(4, null, null);
        Node<Integer> node1 = new Node<Integer>(1, node6, node7);
        Node<Integer> node2 = new Node<Integer>(2, node4, node5);
        Node<Integer> node3 = new Node<Integer>(3, node2, node1);

        Node<Integer> root = BinaryTree2BinarySearchTreeConverter.convert(node3);

        assertEquals(node1, search(root, node1));
        assertEquals(node2, search(root, node2));
        assertEquals(node3, search(root, node3));
        assertEquals(node4, search(root, node4));
        assertEquals(node5, search(root, node5));
        assertEquals(node6, search(root, node6));
        assertEquals(node7, search(root, node7));
    }

    private <T extends Comparable<T>> Node<T> search(Node<T> root, Node<T> value) {
        Node<T> result = null;

        if (root != null && value != null) {
            switch (root.compareTo(value)) {
                case -1:
                    result = search(root.getRight(), value);
                    break;
                case 1:
                    result = search(root.getLeft(), value);
                    break;
                default:
                    result = root;
            }
        }

        return result;
    }
}
