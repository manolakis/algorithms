package com.thejavadeveloper.algorithms.tree;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Red Black Tree Tests.
 * User: manuel.martin
 * Date: 13/01/13
 * Time: 17:50
 */
public class RedBlackTreeTest {

    // - test methods -----------------------------------------------

    @Test
    public void testInsertion() {
        RedBlackTree tree = new RedBlackTree();

        tree.add(3).add(50).add(5).add(1).add(7);

        assertArrayEquals(new int[]{1, 3, 5, 7, 50}, tree.getInOrderTreeWalk());
    }

    @Test
    public void testDeletion() {
        RedBlackTree tree = new RedBlackTree();

        tree
                .add(2).add(1).add(5).add(4).add(3)
                .add(6).add(7).add(8).add(9).add(10)
                .add(11).add(12).add(13).add(14).add(15);

        tree.delete(5);

        assertArrayEquals(new int[]{1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, tree.getInOrderTreeWalk());

        tree.delete(2);

        assertArrayEquals(new int[]{1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, tree.getInOrderTreeWalk());
    }

    @Test
    public void testMinimum() {
        RedBlackTree tree = new RedBlackTree();

        tree.add(3).add(50).add(5).add(1).add(7);

        assertEquals(1, tree.getMinimum());
    }

    @Test
    public void testMaximum() {
        RedBlackTree tree = new RedBlackTree();

        tree.add(3).add(50).add(5).add(1).add(7);

        assertEquals(50, tree.getMaximum());
    }

    @Test
    public void testHasNumber() {
        RedBlackTree tree = new RedBlackTree();

        tree.add(3).add(50).add(5).add(1).add(7);

        assertTrue(tree.hasNumber(5));
        assertFalse(tree.hasNumber(6));
    }

}
