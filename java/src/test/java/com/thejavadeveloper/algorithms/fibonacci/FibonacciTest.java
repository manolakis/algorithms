package com.thejavadeveloper.algorithms.fibonacci;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Fibonacci algorithms tests.
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 02/02/13
 * Time: 11:36
 */
public class FibonacciTest {

    // - instance attributes ----------------------------------------

    private Fibonacci fibonacci = new Fibonacci();

    // - test methods -----------------------------------------------

    @Test
    public void testGetNthRecursive() {
        assertEquals(0, fibonacci.getNthRecursive(0));
        assertEquals(5, fibonacci.getNthRecursive(5));
        assertEquals(55, fibonacci.getNthRecursive(10));
        assertEquals(6765, fibonacci.getNthRecursive(20));
        assertEquals(832040, fibonacci.getNthRecursive(30));
    }

    @Test
    public void testGetNthNonRecursive() {
        assertEquals(0, fibonacci.getNthNonRecursive(0));
        assertEquals(5, fibonacci.getNthNonRecursive(5));
        assertEquals(55, fibonacci.getNthNonRecursive(10));
        assertEquals(6765, fibonacci.getNthNonRecursive(20));
        assertEquals(832040, fibonacci.getNthNonRecursive(30));
    }

    @Test
    public void testGetNthMatrix() {
        assertEquals(0, fibonacci.getNthMatrix(0));
        assertEquals(5, fibonacci.getNthMatrix(5));
        assertEquals(55, fibonacci.getNthMatrix(10));
        assertEquals(6765, fibonacci.getNthMatrix(20));
        assertEquals(832040, fibonacci.getNthMatrix(30));
    }

    @Test
    public void testGetNthFastDoublingRecursive() {
        assertEquals(0, fibonacci.getNthFastDoublingRecursive(0));
        assertEquals(5, fibonacci.getNthFastDoublingRecursive(5));
        assertEquals(55, fibonacci.getNthFastDoublingRecursive(10));
        assertEquals(6765, fibonacci.getNthFastDoublingRecursive(20));
        assertEquals(832040, fibonacci.getNthFastDoublingRecursive(30));
    }

    @Test
    public void testGetNthFastDoublingNonRecursive() {
        assertEquals(0, fibonacci.getNthFastDoublingNonRecursive(0));
        assertEquals(5, fibonacci.getNthFastDoublingNonRecursive(5));
        assertEquals(55, fibonacci.getNthFastDoublingNonRecursive(10));
        assertEquals(6765, fibonacci.getNthFastDoublingNonRecursive(20));
        assertEquals(832040, fibonacci.getNthFastDoublingNonRecursive(30));
    }

}
