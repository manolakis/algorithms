package com.thejavadeveloper.algorithms.primes;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

/**
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 04/02/13
 * Time: 22:41
 */
public class PrimeNumbersTest {

    @Test
    public void testIsPrime() throws Exception {
        PrimeNumbers primeNumbers = new PrimeNumbers();

        assertTrue(primeNumbers.isPrime(2));
        assertTrue(primeNumbers.isPrime(3));
        assertFalse(primeNumbers.isPrime(4));
        assertTrue(primeNumbers.isPrime(5));
        assertFalse(primeNumbers.isPrime(6));
        assertTrue(primeNumbers.isPrime(7));

        assertTrue(primeNumbers.isPrime(127));
        assertFalse(primeNumbers.isPrime(128));

        assertTrue(primeNumbers.isPrime(571));
        assertFalse(primeNumbers.isPrime(573));

        assertTrue(primeNumbers.isPrime(911));
        assertFalse(primeNumbers.isPrime(923));
    }

    @Test
    public void testGetPrimeNumbersUntil() {
        PrimeNumbers primeNumbers = new PrimeNumbers();

        assertArrayEquals(new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29}, primeNumbers.getPrimeNumbersUntil(30));
    }
}
