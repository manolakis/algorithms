package com.thejavadeveloper.algorithms.numbers;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Numbers tests.
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 11:13
 */
public class NumbersTest {

    // - class attributes -------------------------------------------

    private static final int ZERO = 0; // 0
    private static final int ONE = 1; // 1
    private static final int TWO = 2; // 10
    private static final int THREE = 3; // 11
    private static final int FOUR = 4; // 100
    private static final int FIVE = 5; // 101
    private static final int SIX = 6; // 110
    private static final int SEVEN = 7; // 111
    private static final int EIGHT = 8; // 1000
    private static final int NINE = 9; // 1001
    private static final int TEN = 10; // 1010

    // - test methods -----------------------------------------------

    @Test
    public void testIfNumberIsBitwisePalindrome() {
        assertTrue(Numbers.isBitwisePalindrome(ZERO));
        assertTrue(Numbers.isBitwisePalindrome(ONE));
        assertFalse(Numbers.isBitwisePalindrome(TWO));
        assertTrue(Numbers.isBitwisePalindrome(THREE));
        assertFalse(Numbers.isBitwisePalindrome(FOUR));
        assertTrue(Numbers.isBitwisePalindrome(FIVE));
        assertFalse(Numbers.isBitwisePalindrome(SIX));
        assertTrue(Numbers.isBitwisePalindrome(SEVEN));
        assertFalse(Numbers.isBitwisePalindrome(EIGHT));
        assertTrue(Numbers.isBitwisePalindrome(NINE));
        assertFalse(Numbers.isBitwisePalindrome(TEN));
    }
}
