package com.thejavadeveloper.algorithms.graphs;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 16:35
 */
public class DijkstraTest {

    @Test
    public void verifyDijkstraAlgorithm() {
        Node
                node1 = new Node("1"),
                node2 = new Node("2"),
                node3 = new Node("3"),
                node4 = new Node("4"),
                node5 = new Node("5");

        Vertex
                vertex1 = new Vertex(7, node2),
                vertex2 = new Vertex(2, node4),
                vertex3 = new Vertex(1, node3),
                vertex4 = new Vertex(2, node4),
                vertex5 = new Vertex(5, node5),
                vertex6 = new Vertex(3, node2),
                vertex7 = new Vertex(8, node3),
                vertex8 = new Vertex(5, node5),
                vertex9 = new Vertex(4, node3);

        node1.addVertex(vertex1);
        node1.addVertex(vertex2);
        node2.addVertex(vertex3);
        node2.addVertex(vertex4);
        node3.addVertex(vertex5);
        node4.addVertex(vertex6);
        node4.addVertex(vertex7);
        node4.addVertex(vertex8);
        node5.addVertex(vertex9);

        Graph graph = new Graph();
        graph.addAllStructure(node1);

        List<Node> result = new ArrayList<Node>();

        result.add(node1);
        result.add(node4);
        result.add(node2);
        result.add(node3);


        assertThat(result, is(Dijkstra.findBestPath(graph, node1, node3)));
    }
}
