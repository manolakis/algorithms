package com.thejavadeveloper.algorithms.search;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Search tests.
 * User: manuel.martin
 * Date: 19/01/13
 * Time: 19:20
 */
public class SearchTest {

    @Test
    public void searchInSortedRotatedArray() {
        Search search = new Search();

        int[] sortedRotatedArray = new int[]{4, 5, 6, 7, 8, 0, 1, 2, 3};

        assertEquals(1, search.searchInSortedRotatedArray(sortedRotatedArray, 5));
        assertEquals(0, search.searchInSortedRotatedArray(sortedRotatedArray, 4));
        assertEquals(8, search.searchInSortedRotatedArray(sortedRotatedArray, 3));
        assertEquals(5, search.searchInSortedRotatedArray(sortedRotatedArray, 0));
        assertEquals(-1, search.searchInSortedRotatedArray(sortedRotatedArray, 9));
    }
}
