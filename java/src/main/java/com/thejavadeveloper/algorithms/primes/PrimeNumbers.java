package com.thejavadeveloper.algorithms.primes;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 04/02/13
 * Time: 22:20                        
 */
public class PrimeNumbers {

    public boolean isPrime(int number) {
        boolean result = (number > 1);

        if (number > 2) {
            if (number % 2 == 0) {
                // only check even numbers
                result = false;
            } else {
                int max = new Double(Math.sqrt(number)).intValue();
                int[] primes = this.getPrimeNumbersUntil(max);
                for (int i = 0; i < primes.length; i++) {
                    if (number % primes[i] == 0) {
                        result = false;
                        break;
                    }
                }
            }
        }

        return result;
    }

    public int[] getPrimeNumbersUntil(int number) {
        List<Integer> primes = new ArrayList<Integer>();
        primes.add(2);

        nextprime:
        for (int i = 3; i < number; i += 2) {
            for (int j = 0, prime = primes.get(j); prime * prime <= i; ) {
                if (i % prime == 0) {
                    continue nextprime;
                }
                prime = primes.get(++j);
            }

            primes.add(i);
        }

        int[] result = new int[primes.size()];
        for (int i = 0; i < primes.size(); i++) {
            result[i] = primes.get(i);
        }

        return result;
    }
}
