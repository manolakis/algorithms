package com.thejavadeveloper.algorithms.graphs;

import java.util.*;

/**
 * Dijkstra algorithm implementation.
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 16:19
 */
public class Dijkstra {

    public static List<Node> findBestPath(final Graph graph, Node source, Node destiny) {
        final Queue<NodeDecorator> queue = new LinkedList<NodeDecorator>();
        final Map<Node, NodeDecorator> decorators = new HashMap<Node, NodeDecorator>();
        final List<Node> result = new ArrayList<Node>();

        // initialization
        for (Node node : graph.getNodes()) {
            NodeDecorator nodeDecorator = new NodeDecorator(node);

            if (node.equals(source)) {
                nodeDecorator.setWeight(0);
            }

            decorators.put(node, nodeDecorator);
            queue.add(nodeDecorator);
        }
        Collections.sort((List<NodeDecorator>) queue);

        // dijkstra
        NodeDecorator nodeDecorator = queue.poll();
        while (nodeDecorator != null) {
            nodeDecorator.setVisited(true);

            for (Vertex vertex : nodeDecorator.getNode().getVertexes()) {
                NodeDecorator decorator = decorators.get(vertex.getDestiny());

                if (!decorator.isVisited()) {
                    int pathWeight = nodeDecorator.getWeight() + vertex.getWeight();
                    if (decorator.getWeight() == null || decorator.getWeight() > pathWeight) {
                        decorator.setWeight(pathWeight);
                        decorator.setPredecesor(nodeDecorator);
                    }
                }
            }

            Collections.sort((List<NodeDecorator>) queue);
            nodeDecorator = queue.poll();
        }

        // obtain path in reverse order
        nodeDecorator = decorators.get(destiny);

        do {
            result.add(0, nodeDecorator.getNode());
            nodeDecorator = nodeDecorator.getPredecesor();
        } while (nodeDecorator != null);

        return result;
    }

    // - inner classes ----------------------------------------------

    private static class NodeDecorator implements Comparable<NodeDecorator> {

        // - instance attributes ------------------------------------

        private boolean visited;
        private Integer weight;
        private Node node;
        private NodeDecorator predecesor;

        // - constructors -------------------------------------------

        private NodeDecorator(final Node node) {
            this.node = node;
            this.predecesor = null;
            this.weight = null; // null value represents infinite
            this.visited = false;
        }

        // - instance methods ---------------------------------------

        public Node getNode() {
            return node;
        }

        public boolean isVisited() {
            return visited;
        }

        public void setVisited(boolean visited) {
            this.visited = visited;
        }

        public Integer getWeight() {
            return weight;
        }

        public void setWeight(Integer weight) {
            this.weight = weight;
        }

        public NodeDecorator getPredecesor() {
            return predecesor;
        }

        public void setPredecesor(NodeDecorator predecesor) {
            this.predecesor = predecesor;
        }

        /* used in queue */
        @Override
        public int compareTo(NodeDecorator node) {
            int result;

            if (this.getWeight() == null || node.getWeight() == null) {
                result = this.getWeight() == null ? 1 : -1;
            } else {
                result = this.getWeight().compareTo(node.getWeight());
            }

            return result;
        }

    }

}
