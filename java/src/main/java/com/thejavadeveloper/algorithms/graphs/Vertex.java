package com.thejavadeveloper.algorithms.graphs;

/**
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 17:57
 */
public class Vertex {

    // - instance attributes ----------------------------------------

    private int weight;
    private Node destiny;

    // - constructors -----------------------------------------------

    public Vertex(int weight, Node destiny) {
        this.weight = weight;
        this.destiny = destiny;
    }

    // - instance methods -------------------------------------------


    public int getWeight() {
        return weight;
    }

    public Node getDestiny() {
        return destiny;
    }
}
