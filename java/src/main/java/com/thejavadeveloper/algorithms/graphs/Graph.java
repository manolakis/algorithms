package com.thejavadeveloper.algorithms.graphs;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Graph.
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 18:05
 */
public class Graph {

    // - instance attributes ----------------------------------------

    private Set<Node> nodes;
    private Set<Vertex> vertexes;

    // - constructors -----------------------------------------------

    public Graph() {
        this.nodes = new HashSet<Node>();
        this.vertexes = new HashSet<Vertex>();
    }

    // - instance methods -------------------------------------------

    public void addNode(final Node node) {
        this.nodes.add(node);
    }

    public void addVertex(final Vertex vertex) {
        this.vertexes.add(vertex);
    }

    public void addAllStructure(final Node node) {
        if (!this.nodes.contains(node)) {
            this.addNode(node);

            for (Vertex vertex : node.getVertexes()) {
                this.addVertex(vertex);
                this.addAllStructure(vertex.getDestiny());
            }
        }
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public Set<Vertex> getVertexes() {
        return vertexes;
    }
}
