package com.thejavadeveloper.algorithms.graphs;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Node of the Graph.
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 17:55
 */
public class Node {

    // - instance attributes ----------------------------------------

    private String name;
    private Set<Vertex> vertexes;

    // - constructors -----------------------------------------------

    public Node(String name) {
        this.name = name;
        this.vertexes = new HashSet<Vertex>();
    }

    // - instance methods -------------------------------------------


    public String getName() {
        return name;
    }

    public void addVertex(final Vertex vertex) {
        this.vertexes.add(vertex);
    }

    public Set<Vertex> getVertexes() {
        return vertexes;
    }

}
