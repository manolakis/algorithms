package com.thejavadeveloper.algorithms.numbers;

/**
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 09/02/13
 * Time: 11:16
 */
public final class Numbers {

    // - constructors -----------------------------------------------

    private Numbers() {
    }

    // - class methods ----------------------------------------------

    /**
     * Checks if an integer is a bitwise palindrome.
     * <p/>
     * Example:
     * 5 is 101. If bits are reversed continues beeing 101, so is palindrome.
     * 6 is 110. Bits reversed are 011 so is not a bitwise palindrome.
     *
     * @param number integer to check
     * @return
     */
    public static boolean isBitwisePalindrome(final int number) {
        int aux = number;
        int reversed = 0;

        while (aux != 0) {
            reversed <<= 1;
            reversed = reversed ^ (aux & 1);
            aux >>= 1;
        }

        return number == reversed;
    }
}
