package com.thejavadeveloper.algorithms.search;

import java.util.Arrays;

/**
 * User: manuel.martin
 * Date: 18/01/13
 * Time: 12:44
 */
public class Search {

    // - class attributes -------------------------------------------

    private static final int NOT_FOUND = -1;

    // - instance methods -------------------------------------------

    /**
     * Search an element inside a sorted rotated array.
     *
     * @param array   sorted rotated array
     * @param element item to find
     * @return element index or <code>-1</code> if not found
     */
    public int searchInSortedRotatedArray(int[] array, int element) {
        int index = NOT_FOUND;

        if (array.length == 1) {
            if (element == array[0]) {
                index = 0;
            }
        } else if (array.length > 1) {
            int middle = array.length / 2;
            int[] left = Arrays.copyOfRange(array, 0, middle);
            int[] right = Arrays.copyOfRange(array, middle, array.length);

            if (!isRotated(right)) {
                if (right[0] <= element && element <= right[right.length - 1]) {
                    // element is in right sorted zone
                    index = search(right, 0, right.length - 1, element);
                    if (index != NOT_FOUND) {
                        index += middle;
                    }
                } else {
                    // element is in left rotated zone
                    index = searchInSortedRotatedArray(left, element);
                }
            } else {
                if (left[0] <= element && element <= left[left.length - 1]) {
                    // element is in left sorted zone
                    index = search(left, 0, left.length - 1, element);
                } else {
                    // element is in right rotated zone
                    index = searchInSortedRotatedArray(right, element);
                    if (index != NOT_FOUND) {
                        index += middle;
                    }
                }
            }
        }

        return index;
    }

    private int search(int[] array, int left, int right, int value) {
        int index = NOT_FOUND;
        int size = right - left + 1;
        int middle = size / 2;

        if (array[middle] == value) {
            index = middle;
        } else if (size > 1) {
            if (array[middle] > value) {
                index = search(array, left, middle - 1, value);
            } else {
                index = middle + search(array, middle + 1, right, value);
            }
        }

        return left + index;
    }

    private boolean isRotated(int[] array) {
        return array.length > 0 && array[0] > array[array.length - 1];
    }
}
