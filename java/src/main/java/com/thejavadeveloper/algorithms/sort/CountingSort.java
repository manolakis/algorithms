package com.thejavadeveloper.algorithms.sort;

/**
 * Counting Sort algorithm. O(n)
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 15:47
 */
public class CountingSort implements Sorter {

    @Override
    public int[] sort(int[] input) {
        int[] temporal = new int[input.length],
                result = new int[input.length];

        // increment temporal array with the number of occurrences
        for (int i = 0; i < input.length; i++) {
            temporal[input[i]] = ++temporal[input[i]];
        }

        for (int i = 1; i < temporal.length; i++) {
            temporal[i] = temporal[i] + temporal[i - 1];
        }

        for (int i = result.length - 1; i >= 0; i--) {
            result[temporal[input[i]] - 1] = input[i];
            temporal[input[i]]--;
        }


        return result;
    }
}
