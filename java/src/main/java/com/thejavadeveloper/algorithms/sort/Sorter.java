package com.thejavadeveloper.algorithms.sort;

/**
 * Sorter interface.
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 09:52
 */
public interface Sorter {

    /**
     * Sort an array of integers
     * @param input unsorted array of integers
     * @return sorted array of integers
     */
    int[] sort(final int[] input);

}
