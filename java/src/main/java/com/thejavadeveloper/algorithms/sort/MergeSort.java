package com.thejavadeveloper.algorithms.sort;

import java.util.Arrays;

/**
 * MergeSort algorithm. O(n.lg(n))
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 14:37
 */
public class MergeSort implements Sorter {

    @Override
    public int[] sort(int[] input) {
        return mergeSort(input);
    }

    private int[] mergeSort(int[] array) {
        int[] result = array;
        if (array.length > 1) {
            int middle = array.length / 2;

            int[] leftArray = this.mergeSort(Arrays.copyOfRange(array, 0, middle));
            int[] rightArray = this.mergeSort(Arrays.copyOfRange(array, middle, array.length));

            result = this.mergeArrays(leftArray, rightArray);
        }

        return result;
    }

    /**
     * Merge two arrays into one.
     */
    private int[] mergeArrays(final int[] firstArray, final int[] secondArray) {
        final int[] finalArray = new int[firstArray.length + secondArray.length];

        int firstIndex = 0, secondIndex = 0;

        for (int finalIndex = 0; finalIndex < finalArray.length; finalIndex++) {
            if (firstIndex < firstArray.length && secondIndex < secondArray.length) {
                finalArray[finalIndex] = (firstArray[firstIndex] < secondArray[secondIndex]) ? firstArray[firstIndex++] : secondArray[secondIndex++];
            } else {
                finalArray[finalIndex] = firstIndex < firstArray.length ? firstArray[firstIndex++] : secondArray[secondIndex++];
            }
        }

        return finalArray;
    }

}
