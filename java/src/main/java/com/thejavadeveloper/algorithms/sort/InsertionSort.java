package com.thejavadeveloper.algorithms.sort;

/**
 * Insertion sort algorithm. O(n^2)
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 14:29
 */
public class InsertionSort implements Sorter {

    @Override
    public int[] sort(int[] input) {
        return this.insertionSort(input);
    }

    private int[] insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = key;
        }
        return array;
    }

}
