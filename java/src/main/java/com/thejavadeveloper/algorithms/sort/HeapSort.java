package com.thejavadeveloper.algorithms.sort;

/**
 * HeapSort algorithm. O(n.lg(n))
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 09:50
 */
public class HeapSort implements Sorter {

    @Override
    public int[] sort(int[] input) {
        int heapSize = input.length;

        this.buildMaxHeap(input);

        for (int index = input.length - 1; index > 0; index--) {
            this.swapElements(input, 0, index);
            this.maxHeapify(input, 0, --heapSize);
        }

        return input;
    }

    /**
     * Maintains the max-heap property.
     *
     * @param array unsorted array
     * @param index node index in witch maintain the max-heap property
     */
    private void maxHeapify(int[] array, int index, int heapSize) {
        int right = (index + 1) * 2,
                left = right - 1,
                largest;

        if (left < heapSize && array[left] > array[index]) {
            largest = left;
        } else {
            largest = index;
        }

        if (right < heapSize && array[right] > array[largest]) {
            largest = right;
        }

        if (largest != index) {
            this.swapElements(array, index, largest);
            this.maxHeapify(array, largest, heapSize);
        }
    }

    /**
     * Build the MaxHeap
     *
     * @param array unsorted array
     */
    private void buildMaxHeap(int[] array) {
        for (int index = (array.length / 2) - 1; index >= 0; index--) {
            this.maxHeapify(array, index, array.length);
        }
    }

    /**
     * Swap two elements of the array
     *
     * @param array array of elements
     * @param i     index of the first element to swap
     * @param j     index of the second element to swap
     */
    private void swapElements(int[] array, int i, int j) {
        int aux = array[i];
        array[i] = array[j];
        array[j] = aux;
    }
}
