package com.thejavadeveloper.algorithms.sort;

/**
 * QuickSort algorithm. O(n^2) in worst case. O(n.lg(n)) in average.
 * User: manuel.martin
 * Date: 12/01/13
 * Time: 12:34
 */
public class QuickSort implements Sorter {

    @Override
    public int[] sort(int[] input) {
        this.quickSort(input, 0, input.length - 1);
        return input;
    }

    private void quickSort(int[] array, int start, int end) {
        if (start < end) {
            int pivot = this.getPivot(array, start, end);
            this.quickSort(array, start, pivot - 1);
            this.quickSort(array, pivot + 1, end);
        }
    }

    /**
     * Swap two elements of the array
     *
     * @param array array of elements
     * @param i     index of the first element to swap
     * @param j     index of the second element to swap
     */
    private void swapElements(int[] array, int i, int j) {
        int aux = array[i];
        array[i] = array[j];
        array[j] = aux;
    }

    private int getPivot(int[] array, int start, int end) {
        int pivot = array[end],
                i = start,
                j = end;

        while (i < j) {
            if (array[i] > pivot) {
                this.swapElements(array, i, --j);
            } else {
                i++;
            }
        }

        if (j != end) {
            this.swapElements(array, end, i);
        }

        return j;
    }
}
