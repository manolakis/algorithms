package com.thejavadeveloper.algorithms.sort;

/**
 * User: manuel.martin
 * Date: 14/01/13
 * Time: 21:16
 */
public class TwoColorsSort implements Sorter {

    public int[] sort(int[] input) {
        int i = 0;
        byte flag = 0;

        for (int k = 1; k < input.length; k++) {
            while (i < input.length && ((input[i] ^ flag) == 0)) {
                i++;
                flag ^= 1;
            }

            if ((i < k) && ((input[k] ^ flag) == 0)) {
                this.swap(input, i, k);

            }
        }

        return input;
    }

    private void swap(int[] array, int i, int j) {
        int aux = array[i];
        array[i] = array[j];
        array[j] = aux;
    }
}
