package com.thejavadeveloper.algorithms.fibonacci;

/**
 * Different implementations to resolve Fibonacci Nth element.
 * <pre>
 * {@code
 *
 *     F(n) = F(n-1) + F(n-2)
 * }
 * </pre>
 * User: Manuel Martin (manuel.martin@gmail.com))
 * Date: 02/02/13
 * Time: 10:45
 */
public class Fibonacci {

    // - class attributes -------------------------------------------

    private final static long[] MATRIX = new long[]{1, 1, 1, 0};

    // - instance methods -------------------------------------------

    /**
     * Recursive method.
     * <pre>
     * {@code
     *
     *     F(n) = F(n-1) + F(n-2)
     * }
     * </pre>
     */
    public long getNthRecursive(long n) {
        long result;

        if (n == 0) {
            result = 0;
        } else if (n <= 2) {
            result = 1l;
        } else {
            result = this.getNthRecursive(n - 1) + this.getNthRecursive(n - 2);
        }

        return result;
    }

    /**
     * Non recursive method.
     * <pre>
     * {@code
     *
     *     F(n) = F(n-1) + F(n-2)
     * }
     * </pre>
     */
    public long getNthNonRecursive(long n) {
        long result;

        if (n == 0) {
            result = 0;
        } else if (n <= 2) {
            result = 1l;
        } else {
            long a = 1;
            long b = 1;

            for (int i = 3; i < n; i++) {
                long c = a + b;
                a = b;
                b = c;
            }

            result = a + b;
        }

        return result;
    }

    /**
     * Matrix method.
     * <pre>
     * {@code
     *
     *     |1 1|^n   |F(n+1)   F(n)|
     *     |1 0|   = |F(n)   F(n-1)|
     * }
     * </pre>
     */
    public long getNthMatrix(long n) {
        long result;

        if (n == 0) {
            result = 0;
        } else if (n <= 2) {
            result = 1l;
        } else {
            long[] matrix = MATRIX;
            for (int i = 1; i < n; i++) {
                matrix = multiplyMatrix(matrix, MATRIX);
            }

            result = matrix[1];
        }

        return result;
    }

    private static long[] multiplyMatrix(long[] matrix1, long[] matrix2) {
        return new long[] {
                (matrix1[0] * matrix2[0]) + (matrix1[1] * matrix2[2]),
                (matrix1[0] * matrix2[1]) + (matrix1[1] * matrix2[3]),
                (matrix1[2] * matrix2[0]) + (matrix1[3] * matrix2[2]),
                (matrix1[2] * matrix2[1]) + (matrix1[3] * matrix2[3])
        };
    }

    /**
     * Fast doubling method.
     * <pre>
     * {@code
     *
     *     F(2n) = F(n) * (2*F(n+1) - F(n))
     *     F(2n+1) = F(n+1)^2 + F(n)^2
     * }
     * </pre>
     */
    public long getNthFastDoublingRecursive(long n) {
        long result;

        if (n == 0) {
            result = 0;
        } else if (n <= 2) {
            result = 1l;
        } else {

            long k = n / 2;
            long a = this.getNthFastDoublingRecursive(k + 1);
            long b = this.getNthFastDoublingRecursive(k);

            if (n % 2 == 1) {
                result = (a * a) + (b * b);
            } else {
                result = b * (2 * a - b);
            }
        }

        return result;
    }

    /**
     * Fast doubling method.
     * <pre>
     * {@code
     *
     *     F(2n) = F(n) * (2*F(n+1) - F(n))
     *     F(2n+1) = F(n+1)^2 + F(n)^2
     * }
     * </pre>
     *
     * @see <a href="http://nayuki.eigenstate.org/page/fast-fibonacci-algorithms">Fast Fibonacci Algorithms</a>
     */
    public long getNthFastDoublingNonRecursive(long n) {
        long a = 0l;
        long b = 1l;

        for (int i = 31; i >= 0; i--) {
            // Double it
            long d = a * ((2 * b) - a);
            long e = (a * a) + (b * b);

            a = d;
            b = e;

            // Advance by one conditionally
            if (((1 << i) & n) != 0) {
                long c = a + b;
                a = b;
                b = c;
            }
        }
        return a;
    }


}
