package com.thejavadeveloper.algorithms.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * RedBlack Tree
 * User: manuel.martin
 * Date: 13/01/13
 * Time: 20:55
 */
public class RedBlackTree implements Tree {

    // - class attributes -------------------------------------------

    private static final Node NIL = new Node();

    static {
        NIL.parent = NIL;
        NIL.left = NIL;
        NIL.right = NIL;
        NIL.color = Node.Color.NONE;
        NIL.value = -1;
    }

    // - instance attributes ----------------------------------------

    private Node root = NIL;

    // - instance methods -------------------------------------------

    @Override
    public Tree add(int x) {
        Node node = new Node();
        node.color = Node.Color.RED;
        node.value = x;

        Node parent = NIL;
        Node aux = this.root;

        while (!NIL.equals(aux)) {
            parent = aux;
            aux = (node.value < aux.value) ? aux.left : aux.right;
        }

        node.parent = parent;

        if (NIL.equals(parent)) {
            this.root = node;
        } else if (node.value < parent.value) {
            parent.left = node;
        } else {
            parent.right = node;
        }

        node.left = NIL;
        node.right = NIL;
        node.color = Node.Color.RED;

        this.insertFixUp(node);

        return this;
    }

    @Override
    public Tree delete(int x) {
        Node nodeToDelete = this.search(this.root, x);
        Node aux;

        if (!NIL.equals(nodeToDelete)) {

            Node minimum = nodeToDelete;
            Node.Color originalColor = minimum.color;

            if (NIL.equals(nodeToDelete.left)) {
                aux = nodeToDelete.right;
                this.transplant(nodeToDelete, nodeToDelete.right);
            } else if (NIL.equals(nodeToDelete.right)) {
                aux = nodeToDelete.left;
                this.transplant(nodeToDelete, nodeToDelete.left);
            } else {
                minimum = this.getMinimumNode(nodeToDelete.right);
                originalColor = minimum.color;
                aux = minimum.right;

                if (minimum.parent.equals(nodeToDelete)) {
                    aux.parent = minimum;
                } else {
                    this.transplant(minimum, minimum.right);
                    minimum.right = nodeToDelete.right;
                    nodeToDelete.right.parent = minimum;
                }

                this.transplant(nodeToDelete, minimum);
                minimum.left = nodeToDelete.left;
                nodeToDelete.left.parent = minimum;
                minimum.color = nodeToDelete.color;
            }

            if (Node.Color.BLACK.equals(originalColor)) {
                this.deleteFixUp(aux);
            }

        }

        return this;
    }

    private void deleteFixUp(Node node) {
        Node current = node;

        while (!current.equals(this.root) && Node.Color.BLACK.equals(current.color)) {
            if (current.equals(current.parent.left)) {
                Node brother = current.parent.right;

                if (Node.Color.RED.equals(brother.color)) {
                    brother.color = Node.Color.BLACK;
                    current.parent.color = Node.Color.RED;
                    this.leftRotate(current.parent);
                    brother = current.parent.right;
                }

                if (Node.Color.BLACK.equals(brother.left.color) && Node.Color.BLACK.equals(brother.right.color)) {
                    brother.color = Node.Color.RED;
                    current = current.parent;
                } else {

                    if (Node.Color.BLACK.equals(brother.right.color)) {
                        brother.left.color = Node.Color.BLACK;
                        this.rightRotate(brother);
                        brother = current.parent.right;
                    }

                    brother.color = current.parent.color;
                    current.parent.color = Node.Color.BLACK;
                    brother.right.color = Node.Color.BLACK;
                    this.leftRotate(current.parent);
                    current = this.root;
                }
            } else {
                Node brother = current.parent.left;

                if (Node.Color.RED.equals(brother.color)) {
                    brother.color = Node.Color.BLACK;
                    current.parent.color = Node.Color.RED;
                    this.rightRotate(current.parent);
                    brother = current.parent.left;
                }

                if (Node.Color.BLACK.equals(brother.left.color) && Node.Color.BLACK.equals(brother.right.color)) {
                    brother.color = Node.Color.RED;
                    current = current.parent;
                } else {

                    if (Node.Color.BLACK.equals(brother.left.color)) {
                        brother.right.color = Node.Color.BLACK;
                        this.leftRotate(brother);
                        brother = current.parent.left;
                    }

                    brother.color = current.parent.color;
                    current.parent.color = Node.Color.BLACK;
                    brother.left.color = Node.Color.BLACK;
                    this.rightRotate(current.parent);
                    current = this.root;
                }
            }
        }
        current.color = Node.Color.BLACK;
    }

    @Override
    public int getMinimum() {
        Node node = this.getMinimumNode(this.root);
        return (!NIL.equals(node)) ? node.value : -1;
    }

    @Override
    public int getMaximum() {
        Node node = this.getMaximumNode(this.root);
        return (!NIL.equals(node)) ? node.value : -1;
    }

    @Override
    public boolean hasNumber(int number) {
        return !NIL.equals(this.search(this.root, number));
    }

    public int[] getInOrderTreeWalk() {
        List<Integer> list = new ArrayList<Integer>();

        this.getInOrderTreeWalk(list, this.root);
        int[] array = new int[list.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }

        return array;
    }

    private void insertFixUp(Node node) {
        Node current = node;

        while (Node.Color.RED.equals(current.parent.color)) {
            if (current.parent.equals(current.parent.parent.left)) {
                Node uncle = current.parent.parent.right;

                if (Node.Color.RED.equals(uncle.color)) {
                    uncle.color = Node.Color.BLACK;
                    current.parent.color = Node.Color.BLACK;
                    current.parent.parent.color = Node.Color.RED;
                    current = current.parent.parent;
                } else {

                    if (current.equals(current.parent.right)) {
                        current = current.parent;
                        this.leftRotate(current);
                    }

                    current.parent.color = Node.Color.BLACK;
                    current.parent.parent.color = Node.Color.RED;
                    this.rightRotate(current.parent.parent);
                }
            } else {
                Node uncle = current.parent.parent.left;

                if (Node.Color.RED.equals(uncle.color)) {
                    uncle.color = Node.Color.BLACK;
                    current.parent.color = Node.Color.BLACK;
                    current.parent.parent.color = Node.Color.RED;
                    current = current.parent.parent;
                } else {

                    if (current.equals(current.parent.left)) {
                        current = current.parent;
                        this.rightRotate(current);
                    }

                    current.parent.color = Node.Color.BLACK;
                    current.parent.parent.color = Node.Color.RED;
                    this.leftRotate(current.parent.parent);
                }

            }
        }

        this.root.color = Node.Color.BLACK;
    }

    /**
     * Makes a left rotation of a node.
     *
     * @param current Node to rotate
     */
    private void leftRotate(Node current) {
        Node aux = current.right;
        current.right = aux.left;

        if (!NIL.equals(aux.left)) {
            aux.left.parent = current;
        }

        aux.parent = current.parent;
        if (!NIL.equals(current.parent)) {
            if (current.equals(current.parent.left)) {
                current.parent.left = aux;
            } else {
                current.parent.right = aux;
            }
        } else {
            this.root = aux;
        }

        current.parent = aux;
        aux.left = current;
    }

    /**
     * Makes a right rotation of a node.
     *
     * @param current Node to rotate
     */
    private void rightRotate(Node current) {
        Node aux = current.left;
        current.left = aux.right;

        if (!NIL.equals(current.left)) {
            current.left.parent = current;
        }

        aux.parent = current.parent;
        if (!NIL.equals(current.parent)) {
            if (current.equals(current.parent.left)) {
                current.parent.left = aux;
            } else {
                current.parent.right = aux;
            }
        } else {
            this.root = aux;
        }

        current.parent = aux;
        aux.right = current;
    }

    private void transplant(Node current, Node newOne) {
        if (NIL.equals(current.parent)) {
            this.root = newOne;
        } else {
            if (current.equals(current.parent.left)) {
                current.parent.left = newOne;
            } else {
                current.parent.right = newOne;
            }

        }

        newOne.parent = current.parent;
    }

    private Node search(Node node, int value) {
        Node result = node;

        if (!NIL.equals(node) && node.value != value) {
            result = (value < node.value) ? this.search(node.left, value) : this.search(node.right, value);
        }

        return result;
    }

    private Node getMinimumNode(Node node) {
        Node result = node;

        if (!NIL.equals(result)) {
            while (!NIL.equals(result.left)) {
                result = result.left;
            }
        }

        return result;
    }

    private Node getMaximumNode(Node node) {
        Node result = node;

        if (!NIL.equals(result)) {
            while (!NIL.equals(result.right)) {
                result = result.right;
            }
        }

        return result;
    }

    private void getInOrderTreeWalk(List<Integer> list, Node node) {
        if (!NIL.equals(node)) {
            this.getInOrderTreeWalk(list, node.left);
            list.add(node.value);
            this.getInOrderTreeWalk(list, node.right);
        }
    }

    // - inner classes ----------------------------------------------

    private static class Node {

        private static enum Color {NONE, RED, BLACK}

        // - instance attributes ------------------------------------

        private Node parent;
        private Node left;
        private Node right;
        private Color color;
        private int value;

        public Node() {
            this.parent = NIL;
            this.left = NIL;
            this.right = NIL;
            this.color = Color.NONE;
        }

        @Override
        public String toString() {
            return this.value + " - " + this.color.toString();
        }
    }
}
