package com.thejavadeveloper.algorithms.tree;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Binary Search Tree implementation.
 * User: manuel.martin
 * Date: 13/01/13
 * Time: 17:30
 */
public class BinarySearchTree implements Tree, Serializable {

    // - instance attributes ----------------------------------------

    private Node root;

    // - instance methods -------------------------------------------

    @Override
    public BinarySearchTree add(int x) {
        Node newNode = new Node();
        newNode.value = x;

        if (root == null) {
            root = newNode;
        } else {
            Node parent = null,
                    node = this.root;

            while (node != null) {
                parent = node;
                node = (newNode.value < node.value) ? node.left : node.right;
            }

            newNode.parent = parent;
            if (newNode.value < parent.value) {
                parent.left = newNode;
            } else {
                parent.right = newNode;
            }
        }

        return this;
    }

    @Override
    public BinarySearchTree delete(int x) {

        Node node = this.search(this.root, x);
        if (node != null) {

            if (node.left == null) {
                this.transplant(node, node.right);
            } else if (node.right == null) {
                this.transplant(node, node.left);
            } else {
                Node minimum = this.getMinimumNode(node.right);

                if (minimum.parent != node) {
                    this.transplant(minimum, minimum.right);
                    minimum.right = node.right;
                    node.right.parent = minimum;
                }

                this.transplant(node, minimum);
                minimum.left = node.left;
                node.left.parent = minimum;
            }

        }

        return this;
    }

    @Override
    public int getMinimum() {
        Node node = this.getMinimumNode(this.root);
        return (node != null) ? node.value : -1;
    }

    @Override
    public int getMaximum() {
        Node node = this.getMaximumNode(this.root);

        return (node != null) ? node.value : -1;
    }

    @Override
    public boolean hasNumber(int value) {
        return this.search(this.root, value) != null;
    }


    public int[] getInOrderTreeWalk() {
        List<Integer> list = new ArrayList<Integer>();

        this.getInOrderTreeWalk(list, this.root);
        int[] array = new int[list.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }

        return array;
    }

    private void transplant(Node node, Node child) {
        if (node.parent == null) {
            // root
            this.root = child;
        } else {
            if (node == node.parent.left) {
                node.parent.left = child;
            } else {
                node.parent.right = child;
            }
        }

        if (child != null) {
            child.parent = node.parent;
        }
    }

    private void getInOrderTreeWalk(List<Integer> list, Node node) {
        if (node != null) {
            this.getInOrderTreeWalk(list, node.left);
            list.add(node.value);
            this.getInOrderTreeWalk(list, node.right);
        }
    }

    private Node search(Node node, int value) {
        Node result = node;

        if (node != null && node.value != value) {
            result = (value < node.value) ? this.search(node.left, value) : this.search(node.right, value);
        }

        return result;
    }

    private Node getMinimumNode(Node node) {
        Node result = node;

        if (result != null) {
            while (result.left != null) {
                result = result.left;
            }
        }

        return result;
    }

    private Node getMaximumNode(Node node) {
        Node result = node;

        if (result != null) {
            while (result.right != null) {
                result = result.right;
            }
        }

        return result;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        this.serializeTree(objectOutputStream, this.root);
    }

    private void serializeTree(ObjectOutputStream objectOutputStream, Node node) throws IOException {
        if (node != null) {
            objectOutputStream.writeInt(node.value);
            this.serializeTree(objectOutputStream, node.left);
            this.serializeTree(objectOutputStream, node.right);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        while (objectInputStream.available() > 0) {
            int i = objectInputStream.readInt();
            this.add(i);
        }
    }


    // - inner classes ----------------------------------------------

    private class Node {

        // - instance attributes ------------------------------------

        private Node parent;
        private Node left;
        private Node right;
        private int value;

    }
}
