package com.thejavadeveloper.algorithms.tree;

/**
 * Tree interface.
 * User: manuel.martin
 * Date: 13/01/13
 * Time: 17:33
 */
public interface Tree {

    Tree add(int x);

    Tree delete(int x);

    int getMinimum();

    int getMaximum();

    boolean hasNumber(int number);

}
