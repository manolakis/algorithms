package com.thejavadeveloper.algorithms.tree;

import java.util.Collection;

/**
 * Identifies a Trie data structure.
 * User: manuel.martin
 * Date: 22/01/13
 * Time: 22:25
 */
public interface Trie<K, V> {

    /**
     * Inserts a new value inside the Trie and returns the previous TrieEntry if exists.
     *
     * @param key   key that identifies the value
     * @param value value to store
     * @return <code>null</code> or previous TrieEntry if exists.
     */
    TrieEntry<K, V> put(K key, V value);

    /**
     * Given a key returns a Collection of TrieEntry that match it.
     *
     * @param key key to search
     * @return Collection of TrieEntry
     */
    Collection<TrieEntry<K, V>> get(K key);

    /**
     * A Trie entry. These objects are used inside the Trie to store a value and a key that identifies it.
     *
     * @param <K> Type of the key
     * @param <V> Type of the value
     */
    interface TrieEntry<K, V> {

        /**
         * Returns the key
         *
         * @return key
         */
        K getKey();

        /**
         * Returns the value
         *
         * @return value
         */
        V getValue();
    }

}
