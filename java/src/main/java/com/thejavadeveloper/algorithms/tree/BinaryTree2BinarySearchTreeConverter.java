package com.thejavadeveloper.algorithms.tree;

/**
 * In place Binary Tree to Binary Search Tree converter.
 * User: manuel.martin
 * Date: 17/01/13
 * Time: 21:53
 */
public final class BinaryTree2BinarySearchTreeConverter {

    // - constructors -----------------------------------------------

    private BinaryTree2BinarySearchTreeConverter() {
        // hidden constructor for utility class
    }

    // - class methods ----------------------------------------------


    /**
     * Converts a binary tree into a binary search tree.
     * <pre>
     * Binary Search Tree
     *
     *   B
     *  / \
     * A   C
     *
     * A < B <= C
     * </pre>
     *
     * @param node node
     * @param <T>
     */
    public static <T extends Comparable<T>> Node<T> convert(Node<T> node) {
        Node<T> doubleLinkedList = convertToDoubleLinkedList(node);
        doubleLinkedList = sort(doubleLinkedList);

        return makeBST(doubleLinkedList);
    }

    private static <T extends Comparable<T>> Node<T> makeBST(Node<T> node) {
        Node result = getMiddle(node);

        // left side
        if (result.left != null) {
            result.left.right = null;
            result.left = makeBST(result.left);
        }

        // right side
        if (result.right != null) {
            result.right.left = null;
            result.right = makeBST(result.right);
        }


        return result;
    }

    private static <T extends Comparable<T>> int getSize(Node<T> node) {
        Node result = leftNode(node);
        int counter = 0;

        while (result != null) {
            result = result.right;
            counter++;
        }

        return counter;
    }

    private static <T extends Comparable<T>> Node<T> getMiddle(Node<T> node) {
        Node result = node;
        int size = getSize(result);

        if (size > 1) {
            int middle = size / 2;

            result = leftNode(result);

            while (middle > 1) {
                result = result.right;
                middle--;
            }
        }

        return result;
    }

    private static <T extends Comparable<T>> Node<T> sort(Node<T> node) {
        Node<T> result = leftNode(node);

        if (result != null) {
            Node<T> index = result;

            while (index.right != null) {
                index = index.right;
                result = index;

                while (result.left != null && result.compareTo(result.left) < 0) {
                    if (index.equals(result)) {
                        index = result.left;
                    }
                    swap(result.left, result);
                }
            }
        }

        return leftNode(result);
    }


    /**
     * Convert the binary tree to a double linked list
     *
     * @param node binary tree root node
     * @param <T>  Type of tree elements
     * @return First element of the double linked list
     */
    private static <T extends Comparable<T>> Node<T> convertToDoubleLinkedList(Node<T> node) {
        Node<T> result = node;
        if (node != null) {
            Node<T> left = convertToDoubleLinkedList(node.left);
            Node<T> right = convertToDoubleLinkedList(node.right);
            Node<T> leftRightNode = rightNode(left);
            Node<T> rightLeftNode = leftNode(right);

            node.left = leftRightNode;
            if (leftRightNode != null) {
                leftRightNode.right = node;
            }
            node.right = rightLeftNode;
            if (rightLeftNode != null) {
                rightLeftNode.left = node;
            }

            result = (left != null) ? left : node;

        }
        return result;
    }

    private static <T extends Comparable<T>> Node<T> leftNode(Node<T> node) {
        Node<T> result = node;
        if (node != null) {
            while (result.left != null) {
                result = result.left;
            }
        }
        return result;
    }

    private static <T extends Comparable<T>> Node<T> rightNode(Node<T> node) {
        Node<T> result = node;
        if (node != null) {
            while (result.right != null) {
                result = result.right;
            }
        }
        return result;
    }


    private static <T extends Comparable<T>> void swap(Node<T> nodeA, Node<T> nodeB) {
        nodeA.right = nodeB.right;
        nodeB.left = nodeA.left;

        if (nodeA.right != null) {
            nodeA.right.left = nodeA;
        }
        if (nodeB.left != null) {
            nodeB.left.right = nodeB;
        }

        nodeA.left = nodeB;
        nodeB.right = nodeA;
    }


    // - inner classes ----------------------------------------------

    public static class Node<T extends Comparable<T>> implements Comparable<Node<T>> {

        // - instance attributes ------------------------------------

        private Node left;
        private Node right;
        private T value;

        // - constructors -------------------------------------------

        public Node(T value, Node<T> left, Node<T> right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        // - instance methods ---------------------------------------

        public Node<T> getLeft() {
            return this.left;
        }

        public Node<T> getRight() {
            return this.right;
        }

        @Override
        public int compareTo(Node<T> o) {
            return this.value.compareTo(o.value);
        }

        @Override
        public String toString() {
            return "[" + this.value.toString() + "]";
        }
    }
}
