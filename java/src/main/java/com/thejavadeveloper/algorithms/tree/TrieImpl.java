package com.thejavadeveloper.algorithms.tree;

import java.util.*;

/**
 * Trie implementation {@link http://en.wikipedia.org/wiki/Trie}.
 * User: manuel.martin
 * Date: 22/01/13
 * Time: 23:04
 *
 * @param <K> Type of keys
 * @param <V> Type of values
 * @param <Z> Type of prefixes used for identify the TrieNodes
 */
public class TrieImpl<K, V, Z> implements Trie<K, V> {

    // - instance attributes ----------------------------------------

    private PrefixSelector<K, Z> prefixSelector;
    private TrieNode<K, V, Z> root;

    // - constructors -----------------------------------------------

    /**
     * Constructs a new instance of a TrieImpl that uses a PrefixSelector to transform the keys into prefixes to
     * identify the internal nodes of the Trie.
     *
     * @param prefixSelector PrefixSelector used to transforms the keys into prefixes
     */
    public TrieImpl(PrefixSelector<K, Z> prefixSelector) {
        this.root = new TrieNode<K, V, Z>(null);
        this.prefixSelector = prefixSelector;
    }

    // - instance methods -------------------------------------------

    @Override
    public TrieEntry<K, V> put(K key, V value) {
        final List<Z> prefixes = this.prefixSelector.getPrefixes(key);
        final TrieEntry<K, V> newTrieEntry = new TrieEntryImpl<K, V>(key, value);
        TrieNode<K, V, Z> trieNode = this.root;

        for (int i = 0; i < prefixes.size(); i++) {
            Z prefix = prefixes.get(i);
            if (trieNode.getChildren().containsKey(prefix)) {
                trieNode = trieNode.getChildren().get(prefix);
            } else {
                TrieNode<K, V, Z> newTrieNode = new TrieNode<K, V, Z>(prefix);
                trieNode.addChild(newTrieNode);
                trieNode = newTrieNode;
            }
        }

        return trieNode.setTrieEntry(newTrieEntry);
    }

    @Override
    public Collection<TrieEntry<K, V>> get(K key) {
        return this.get(this.root, this.prefixSelector.getPrefixes(key), 0);
    }

    private Collection<TrieEntry<K, V>> get(TrieNode<K, V, Z> trieNode, List<Z> prefixes, int index) {
        assert index < prefixes.size();
        Collection<TrieEntry<K, V>> result = new ArrayList<TrieEntry<K, V>>();

        Z prefix = prefixes.get(index);
        TrieNode<K, V, Z> childTrieNode = trieNode.getChildren().get(prefix);

        if (childTrieNode != null) {
            if (index < prefixes.size() - 1) {
                result.addAll(this.get(childTrieNode, prefixes, index + 1));
            } else {
                // final node found
                result.addAll(this.addAllChildren(childTrieNode));
            }
        }

        return result;
    }

    private Collection<TrieEntry<K, V>> addAllChildren(TrieNode<K, V, Z> trieNode) {
        Collection<TrieEntry<K, V>> result = new ArrayList<TrieEntry<K, V>>();

        if (trieNode.getTrieEntry() != null) {
            result.add(trieNode.getTrieEntry());
        }

        for (TrieNode<K, V, Z> child : trieNode.getChildren().values()) {
            result.addAll(this.addAllChildren(child));
        }

        return result;
    }


    // - inner classes ----------------------------------------------

    /**
     * Interface to decompose the key into prefixes.
     *
     * @param <K> Type of key
     * @param <Z> Type of prefixes
     */
    public interface PrefixSelector<K, Z> {

        /**
         * Given a key returns a List of prefixes that identifies the key inside the Trie.
         *
         * @param key Key
         * @return List of prefixes
         */
        List<Z> getPrefixes(K key);
    }

    /**
     * Represents a Node of the Trie.
     */
    private static class TrieNode<K, V, Z> {

        // - instance attributes ------------------------------------

        private Z prefix;
        private TrieEntry<K, V> trieEntry;
        private SortedMap<Z, TrieNode<K, V, Z>> children;

        // - constructors -------------------------------------------

        public TrieNode(final Z prefix) {
            this.prefix = prefix;
            this.children = new TreeMap<Z, TrieNode<K, V, Z>>();
        }

        // - instance methods ---------------------------------------

        public void addChild(TrieNode<K, V, Z> trieNode) {
            this.children.put(trieNode.prefix, trieNode);
        }

        public TrieEntry<K, V> setTrieEntry(TrieEntry<K, V> trieEntry) {
            final TrieEntry<K, V> oldTrieEntry = this.trieEntry;
            this.trieEntry = trieEntry;

            return oldTrieEntry;
        }

        public SortedMap<Z, TrieNode<K, V, Z>> getChildren() {
            return children;
        }

        public TrieEntry<K, V> getTrieEntry() {
            return trieEntry;
        }

    }

    /**
     * TrieEntry implementation. For internal use of the Trie.
     *
     * @param <K> Type of the key
     * @param <V> Type of the value
     */
    public static class TrieEntryImpl<K, V> implements TrieEntry<K, V> {

        // - instance attributes ------------------------------------

        private K key;
        private V value;

        // - constructors -------------------------------------------

        public TrieEntryImpl(K key, V value) {
            this.key = key;
            this.value = value;
        }

        // - instance methods ---------------------------------------


        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }
    }

}
