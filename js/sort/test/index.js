var expect = require('expect.js'),
	sorter = require('..'),

	sorterTest = function(sorterName, sorter) {
		return describe(sorterName, function() {
			it('should sort an unsorted int array', function(done) {
				expect(sorter([2,5,4,1,3]).join(',')).to.equal('1,2,3,4,5');
				done();
			});
			it('should sort a sorted int array', function(done) {
				expect(sorter([1,2,3,4,5]).join(',')).to.equal('1,2,3,4,5');
				done();
			});
		});
	};

describe('swap', function() {
	it('must be defined', function(done) {
		expect(Array.prototype.swap).not.to.be(undefined);
		done();
	});
	it('should swap two array elements', function(done) {
		expect([1,2,3].swap(0,2).join(',')).to.equal('3,2,1');
		done();
	});
});

sorterTest('mergeSort', sorter.mergeSort);
sorterTest('heapSort', sorter.heapSort);
