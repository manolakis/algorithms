(function(global){

	if (typeof Array.prototype.swap === 'undefined') {
		Array.prototype.swap = function(idx1, idx2) {
			var aux = this[idx1];

 			this[idx1] = this[idx2];
 			this[idx2] = aux;

 			return this;
		}
	}

		/**
		 * Merge Sort O(n.log(n))
		 */
	var mergeSort = function(array) {
			var mergeArray = function(array1, array2) {
					var resultArray = [];

					while (array1.length > 0 || array2.length > 0) {
						if (array1.length > 0 && array2.length > 0) {
							resultArray.push(array1[0] < array2[0] ? array1.shift() : array2.shift());
						} else {
							resultArray.push(array1.length > 0 ? array1.shift() : array2.shift());
						}
					}

					return resultArray;
				},
				sort = function(array) {
					var result = array,
						leftArray, rightArray, middle;

					if (array.length > 1) {
						middle = array.length / 2;
						leftArray = sort(array.slice(0, middle));
						rightArray = sort(array.slice(middle));

						result = mergeArray(leftArray, rightArray);
					}

					return result;
				}

			return sort(array);
		},

		heapSort = function(array) {
			var maxHeapify = function(array, index, heapSize) {
					var right = (index + 1) * 2,
						left = right - 1,
						largest = (left < heapSize && array[left] > array[index]) ? left : index;

					if (right < heapSize && array[right] > array[largest]) {
						largest = right;
					}

					if (largest !== index) {
						array.swap(index, largest);
						maxHeapify(array, largest, heapSize);
					}
				},
				buildMaxHeap = function(array) {
					var index = (parseInt(array.length / 2), 10) -1;
					
					while (index >= 0) {
						maxHeapify(array, index, array.length);
						index--;
					}
				},
				sort = function(array) {
					var heapSize = array.length,
						index = array.length - 1;

					buildMaxHeap(array);

					while (index > 0) {
						array.swap(0, index);
						maxHeapify(array, 0, --heapSize);
						index--;
					}

					return array;
				}

			return sort(array);
		};

	global.mergeSort = mergeSort;
	global.heapSort = heapSort;

})(typeof exports !== 'undefined' ? module.exports : window);
