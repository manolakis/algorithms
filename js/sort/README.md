# sorter

Sort Algorithms

[![build status](https://secure.travis-ci.org//sorter.png)](http://travis-ci.org//sorter)

## Installation

This module is installed via npm:

``` bash
$ npm install sorter
```

## Example Usage

``` js
var sorter = require('sorter');
```
